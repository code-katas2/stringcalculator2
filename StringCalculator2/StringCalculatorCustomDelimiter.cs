﻿using System;
using System.Collections.Generic;

namespace StringCalculator2
{
    public class StringCalculatorCustomDelimiter:IStringCalculator
    {
        private const char newLine = '\n';
        private const char comma = ',';
        private const char hash = '#';
        private const char leftSquare = '[';
        private const char rightSquare = ']';
        private const string letters ="abcdefghij" ;
        private const char lessThan = '<';
        private const char greaterThan = '>';
        private const string doubleGreaterThan = ">>";
        private const string doubleLessThan = "<<";

        public int Subtract(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var separatorArray = GetSeparators(numbers);
            var delimiterArray = GetDelimiters(numbers, separatorArray);
            var numberslist = GetNumbers(numbers,delimiterArray,separatorArray);

            ValidateNumbers(numberslist);

            var difference = GetDifference(numberslist);

            return difference;
        }

        private string[] GetSeparators(string numbers)
        {
            var separatorArray = new string[] { leftSquare.ToString(),rightSquare.ToString()};

            if (numbers.Contains(doubleLessThan) || numbers.Contains(doubleGreaterThan))
            {
                separatorArray = new string[] { lessThan.ToString(), greaterThan.ToString() };

                return separatorArray;
            }

            if (numbers.StartsWith(lessThan.ToString()))
            {
                numbers = string.Concat(numbers.Split(lessThan));
                separatorArray = numbers.Substring(0, numbers.IndexOf(hash)).Split(greaterThan);
            }

            return separatorArray;
        }
        
        private string[] GetDelimiters(string numbers,string[] separatorArray)
        {
            var delimiterArray = new string[] { comma.ToString(),newLine.ToString()};

            if (numbers.StartsWith(hash.ToString()))
            {
                numbers = string.Concat(numbers.Split(hash));
                delimiterArray = new string[] { numbers.Substring(0, numbers.IndexOf(newLine)) };
            }

            if (numbers.Contains(leftSquare.ToString()))
            {
                delimiterArray = numbers.Substring(0, numbers.IndexOf(newLine)).Split(leftSquare, rightSquare);
            }

            if (numbers.StartsWith(lessThan.ToString()))
            {
                var hashPosition = numbers.IndexOf(hash);
                var newLinePosition = numbers.IndexOf(newLine);
                var delimiters = numbers.Substring(hashPosition+2,newLinePosition-hashPosition-2);
                
                delimiterArray = delimiters.Split(separatorArray,StringSplitOptions.RemoveEmptyEntries);
            }

            return delimiterArray;
        }

        private List<int> GetNumbers(string numbers, string[] delimiterArray, string[] separators)
        {
            var numbersList = new List<int>();

            if (delimiterArray.Length>2&&!delimiterArray[1].Contains(hash.ToString()))
            {
                numbers = string.Concat(numbers.Split(hash));
            } 

            numbers = string.Concat(numbers.Split(leftSquare, rightSquare, newLine, lessThan, greaterThan));
            numbers = string.Concat(numbers.Split(separators,StringSplitOptions.RemoveEmptyEntries));

            foreach (var number in numbers.Split(delimiterArray, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    var convertedNumber = int.Parse(number);

                    numbersList.Add(convertedNumber);
                }
                catch
                {
                    var letterNumber = letters.IndexOf(number);

                    if (letterNumber >= 0)
                    {
                        numbersList.Add(letterNumber);
                    }
                }
            }

            return numbersList;
        }

        private void ValidateNumbers(List<int>numberslist)
        {
            var numbersGreaterThanThousand = string.Empty;

            foreach (var number in numberslist)
            { 
                if (number>1000)
                {
                    numbersGreaterThanThousand = string.Join(" ", numbersGreaterThanThousand, number);
                }
            }

            if (!string.IsNullOrEmpty(numbersGreaterThanThousand))
            {
                throw new Exception("Numbers greater than thousand."+numbersGreaterThanThousand);
            }
        }
        
        private int GetDifference(List<int> numberslist)
        {
            var difference = 0;

            foreach(var number in numberslist)
            {
                if (number>0)
                {
                    difference += number * -1;
                }
                else
                {
                    difference += number;
                }
            }

            return difference;
        }

    }
}
