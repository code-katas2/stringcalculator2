﻿
namespace StringCalculator2
{
    public class StringCalculator : IStringCalculator
    {
        private const char comma = ',';
        private const char newLine = '\n';

        public int Subtract(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var difference = GetDifference(numbers);

            return difference;
        }

        private int GetDifference(string numbers)
        {
            var difference = 0;

            foreach (var number in numbers.Split(comma, newLine))
            {
                difference += int.Parse(number) * -1;
            }

            return difference;
        }


    }
}
