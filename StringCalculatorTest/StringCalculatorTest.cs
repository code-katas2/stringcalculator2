﻿using NUnit.Framework;
using StringCalculator2;
using System;

namespace StringCalculatorTest
{
    [TestFixture]
    public class StringCalculatorTest
    {
        private IStringCalculator _calculator;

        [Test]
        public void GivenEmptyString_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = 0;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Subtract("");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenOneNumber_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -1;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Subtract("1");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenTwoNumbers_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -3;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Subtract("1,2");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenUnlimitedNumbers_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Subtract("1,2,3");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenNewLineAsDelimiter_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Subtract("1\n2,3");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenCustomDelimiter_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -3;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("##;\n1;2");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenNegativeNumbers_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -12;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("##;\n10;-2");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenNumbersGreaterThanThousand_WhenSubtracting_ThenThrowException()
        {
            //Arrange
            var expected = "Numbers greater than thousand. 1001 2000";
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var exception =Assert.Throws<Exception>(()=> _calculator.Subtract("##;\n1;1001;2000"));

            //Assert
            Assert.AreEqual(expected,exception.Message);
        }

        [Test]
        public void GivenAnyLengthDelimiters_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("##[***]\n1***2***3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenMultipleDelimiters_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("##[*][%]\n1*2%3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenAnyLengthMultipleDelimiters_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("##[*#$][%$$]\n1*#$2%$$3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenOneLetterAsNumber_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = 0;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("a");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenTwoLettersAsNumbers_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -1;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("a,b");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenInvalidLetterAsNumber_WhenSubtracting_ThenIgnoreLetter()
        {
            //Arrange
            var expected = -17;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("i,j,k");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenDelimiterSeparators_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("<(>)##(*)\n1*2*3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenUnlimitedNumbersWithDelimiterSeparators_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -9;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("<{>}##{%}\n2%3%4");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenDifferentDelimitersWithFlagsAsSeparators_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -12;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("<[>}##[::}\n3::4::5");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenFlagsAsDelimiterSeparators_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -15;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("<>><##>&<\n4&5&6");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenFlagsAsDelimiterSeparatorsForMultipleDelimiters_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -18;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("<<>>##<$$$><###>\n5$$$6###7");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void tes()
        {
            //Arrange
            var expected = -18;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Subtract("<<>>##<$$$><###>\n5$$$6###7");

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
